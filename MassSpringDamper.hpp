#ifndef MASSSPRINGDAMPER_HPP
#define MASSSPRINGDAMPER_HPP

/**
 * @file MassSpringDamper.hpp
 * @brief Defines the MassSpringDamper class for mass-spring-damper physics.
 *
 * Contains the mass-spring-damper class which is used in the DelaunayMassSpringDamper plugin.
 */

#include <mecacell/mecacell.h>
#include <mecacell/utilities/logger.hpp>

/**
 * @class MassSpringDamper
 * @brief Class for managing mass-spring-damper physics.
 * 
 * @tparam cell_t Type of the cell.
 */
template <typename cell_t>
class MassSpringDamper {

private:
    std::pair<cell_t*, cell_t*> connection; /**< Pair of cells that are connected */
    float_t stiffness; /**< Spring's stiffness in ng/s^2 */
    float_t dampCoef; /**< Spring's damping coefficient in ng/s */
    float_t restLength; /**< Spring's rest length in µm */
    float_t length; /**< Spring's current length in µm */
    MecaCell::Vec direction; /**< Spring's current direction from node 0 to 1 */

    /**
     * @brief Updates the current length and direction.
     * 
     * @param p0 Position of the first cell.
     * @param p1 Position of the second cell.
     */
    void updateLengthDirection(const MecaCell::Vec &p0, const MecaCell::Vec &p1) {
        direction = p1 - p0;
        length = direction.length();
        if (length > 0) direction /= length;
    }

    /**
     * @brief Computes the total speed from the connected cells.
     * 
     * @return The cumulative speed of the cells.
     */
    double computeSpeedFromCells() {
        double sp1 = connection.first->getBody().getVelocity().dot(direction);
        double sp2 = -connection.second->getBody().getVelocity().dot(direction);
        return -(sp1 + sp2);
    }

public:
    /**
     * @brief Default constructor.
     */
    inline MassSpringDamper() = default;

    /**
     * @brief Constructor with parameters.
     * 
     * @param c1 Pointer to the first cell.
     * @param c2 Pointer to the second cell.
     * @param K Spring stiffness.
     * @param C Damping coefficient.
     * @param L Rest length.
     */
    inline MassSpringDamper(cell_t* c1, cell_t* c2, const float_t &K, const float_t &C, const float_t &L)
        : connection(c1, c2), stiffness(K), dampCoef(C), restLength(L), length(L) {}

    /**
     * @brief Sets the rest length of the spring.
     * 
     * @param L Rest length.
     */
    inline void setRestLength(float_t L) { restLength = L; }

    /**
     * @brief Gets the rest length of the spring.
     * 
     * @return Rest length.
     */
    inline float_t getRestLength() const { return restLength; }

    /**
     * @brief Gets the current length of the spring.
     * 
     * @return Current length.
     */
    inline float_t getLength() const { return length; }

    /**
     * @brief Gets the connection between the cells.
     * 
     * @return Pair of connected cells.
     */
    inline std::pair<cell_t *, cell_t *> getConnection() const { return connection; }

    /**
     * @brief Computes the forces for the connected cells.
     */
    void computeForces() {
        // BASIC SPRING
        updateLengthDirection(connection.first->getBody().getPosition(),
                              connection.second->getBody().getPosition());

        float_t x = length - restLength;  // Actual compression or elongation
        bool compression = x < 0;
        double v = computeSpeedFromCells();

        float_t f = (-stiffness * x - dampCoef * v) / 2.0;
        connection.first->getBody().receiveForce(f, -direction, compression);
        connection.second->getBody().receiveForce(f, direction, compression);
    }

    /**
     * @brief Gets the connection as a string.
     * 
     * @return String representation of the connection.
     */
    inline std::string toString() {
        return std::to_string(connection.first->id) + " <-> " + std::to_string(connection.second->id);
    }
};

#endif // MASSSPRINGDAMPER_HPP
