#ifndef BODYMASSSPRINGDAMPER_HPP
#define BODYMASSSPRINGDAMPER_HPP

#define MOVABLE

/**
 * @file BodyDelaunayMassSpringDamper.hpp
 * @brief Defines the BodyDelaunayMassSpringDamper class for Delaunay triangulation and mass-spring-based physics.
 *
 * Contains the body class which works with the DelaunayMassSpringDamper plugin.
 */

#include <mecacell/mecacell.h>
#include <mecacell/movable.h>
#include "PluginDelaunayMassSpringDamper.hpp"
#include "../../../../src/core/BaseBody.hpp"
#include <math.h>

/**
 * @namespace DelaunayMassSpringDamper
 * @brief Namespace for Delaunay triangulation and mass-spring-based physics-related classes and functions.
 */
namespace DelaunayMassSpringDamper {

    /**
     * @class BodyDelaunayMassSpringDamper
     * @brief Class for managing Delaunay triangulation and mass-spring-based physics.
     * 
     * @tparam cell_t Type of the cell.
     * @tparam plugin_t Type of the plugin.
     */
    template<typename cell_t, class plugin_t>
    class BodyDelaunayMassSpringDamper : public MecaCell::Movable, public virtual BaseBody<plugin_t> {

    private:
        double baseRadius; /**< Radius in µm */
        double radius; /**< Radius in µm */
        double density; /**< Density in ng/µm³ */
        double adhesion; /**< Adhesion coefficient */

    public:
        /**
         * @brief Constructor.
         *
         * Initializes the radius to 10 µm and the density to the water density.
         *
         * @param pos Initial position of the body.
         */
        explicit BodyDelaunayMassSpringDamper(const MecaCell::Vector3D &pos = MecaCell::Vector3D::zero()) {
            baseRadius = 10.0; // 10 µm radius
            radius = baseRadius;
            density = 0.001; // 0.001 ng/µm³ density of water ~ 1000 kg/m³
            adhesion = 0.75;
            this->setMass(density * 4 * M_PI / 3 * pow(radius, 3)); // mass = density * 4π/3 * radius³ ng
            this->setPosition(pos);
        }

        /**
         * @brief Gets the bounding box radius.
         * @return Radius.
         */
        inline double getBoundingBoxRadius() const { return radius; }

        /**
         * @brief Sets the radius.
         *
         * Changes the mass knowing the current density.
         *
         * @param rad Radius.
         */
        inline void setRadius(double rad) {
            radius = rad;
            this->setMass(density * 4 * M_PI / 3 * pow(radius, 3)); // mass = density * 4π/3 * radius³ ng
        }

        /**
         * @brief Gets the radius.
         * @return Radius.
         */
        inline double getRadius() { return radius; }

        /**
         * @brief Gets the base radius.
         * 
         * @return Base radius.
         */
        inline double getBaseRadius() const { return baseRadius; }

        /**
         * @brief Sets the base radius.
         * 
         * @param _baseRadius Base radius.
         */
        inline void setBaseRadius(double _baseRadius) { baseRadius = _baseRadius; }

        /**
         * @brief Increases the radius so that the volume is multiplied by 1 + delta.
         * 
         * @param delta Volume augmentation ratio.
         */
        inline void growth(double delta) {
            this->setRadius(baseRadius * std::cbrt(1 + delta));
        }

        /**
         * @brief Sets the density.
         *
         * Changes the mass knowing the current radius.
         *
         * @param d Density.
         */
        inline void setDensity(double d) {
            density = d;
            this->setMass(density * 4 * M_PI / 3 * pow(radius, 3)); // mass = density * 4π/3 * radius³ ng
        }

        /**
         * @brief Gets the adhesion coefficient.
         * @return Adhesion coefficient.
         */
        inline double getAdhesion() const { return adhesion; }

        /**
         * @brief Sets the adhesion coefficient.
         * @param a Adhesion coefficient.
         */
        inline void setAdhesion(double a) { adhesion = a; }

        /**
         * @brief Moves a cell to position v.
         * @param v Position vector.
         */
        inline void moveTo(const MecaCell::Vec &v) { this->setPosition(v); }
    };
}

#endif // BODYMASSSPRINGDAMPER_HPP